def isIn(char, aStr):
    '''
    char: a single character
    aStr: an alphabetized string

    returns: True if char is in aStr; False otherwise
    '''
    # Your code here

    if len(aStr) == 0:
        return False
    if len(aStr) >= 1:
        length = len(aStr) // 2
        temp = aStr[length]

        if char == temp:
            return True
        elif char > temp:
            return isIn(char, aStr[length + 1:])
        elif char < temp:
            return isIn(char, aStr[:length])
        else:
            return False
    else:
        return True


"""
Test Cases
"""
print isIn('a', '')
print isIn('j', 'bcfggghhjlmmost')
print isIn('y', 'ky')
print isIn('u', 'abbhiknprsstv')
print isIn('f', 'dhhjlmnoptty')
print isIn('d', 'bfgikmnnprtuuvvwz')
print isIn('y', 'eiswx')
print isIn('r', 'abgijjnopttxy')
print isIn('q', 'bccfggjjmnprssswwwxy')
print isIn('d', 'acdddikopqrruvwxz')
