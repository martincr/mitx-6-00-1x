def gcdRecur(a, b):
    '''
    a, b: positive integers

    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here

    if b == 0:
        return a
    else:
        return gcdRecur(b,a%b)

'''

'''

# Test cases:

print gcdRecur(2, 12)
print gcdRecur(6, 12)
print gcdRecur(9, 12)
print gcdRecur(17, 12)

"""
print gcdIter(8, 9)
print gcdIter(9, 57)
print gcdIter(13, 6)
print gcdIter(32, 64) #
print gcdIter(24, 114)
print gcdIter(105, 105) #
print gcdIter(24, 18)
print gcdIter(96, 288) #
print gcdIter(192, 208)
print gcdIter(450, 288)
"""
