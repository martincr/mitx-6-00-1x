def gcdIter(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here

    if a < b:
        t = a
        while a%t != 0 or b%t != 0:
            t = t - 1
        return t
    else:
        t = b
        while b%t != 0 or a%t != 0:
            t = t - 1
        return t

'''
1. One easy way to do this is to begin with a test value equal to the smaller of the two input arguments, and iteratively reduce this test value by 1 until you either reach a case where the test divides both a and b without remainder, or you reach 1.
test value = smaller of a or b

2. iteratively reduce this by 1
3. until either case divides a or b without a remainder
4. or you reach 1
'''

# Test cases:

print gcdIter(8, 9)
print gcdIter(9, 57)
print gcdIter(13, 6)
print gcdIter(32, 64) #
print gcdIter(24, 114)
print gcdIter(105, 105) #
print gcdIter(24, 18)
print gcdIter(96, 288) #
print gcdIter(192, 208)
print gcdIter(450, 288)